﻿using System;
using System.Drawing;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;
using static System.Collections.Specialized.BitVector32;
using System.Data.SqlClient;


public class Exercise16
{
    public static object FormsAuthentication { get; private set; }

    public static void Main()
    {
        bool programIsrunning = true;
        int attempts = 0;
        var password = string.Empty;
        var username = string.Empty;
        int nikeschoenen = 40;
        int lacosteschoenen = 60;
        int adidasschoenen = 80;
        bool aut = false;
        string[,] schoenenlijst = new string[3, 7] {
                { "1", "Nike", "Air max", "42", "wit",nikeschoenen.ToString(), "110,99" },
                { "2", "Adidas", "Ozwego", "42", "zwart",adidasschoenen.ToString(), "100,00" },
                { "3", "Lacoste", "t-Clip ", "40", "zwart",lacosteschoenen.ToString(), "120,00" }
};
        while (attempts < 3)
        {

            Console.WriteLine("Enter your Username : ");
            string Username = Console.ReadLine();
            Console.WriteLine("Enter your password: ");

            ConsoleKey key;
            do
            {
                var keyInfo = Console.ReadKey(intercept: true);
                key = keyInfo.Key;

                if (key == ConsoleKey.Backspace && password.Length > 0)
                {
                    Console.Write("\b \b");
                    password = password[0..^1];
                }
                else if (!char.IsControl(keyInfo.KeyChar))
                {
                    Console.Write("*");
                    password += keyInfo.KeyChar;
                }
            } while (key != ConsoleKey.Enter);
            if ((Username == "Mo" && password == "Mo123"))
            {
                Console.WriteLine("");
                Console.WriteLine("Je bent ingelogd");
                programIsrunning = true;
                while (programIsrunning == true)
                {

                    Console.Clear();
                    Console.WriteLine("Maak je keuze:");
                    Console.WriteLine("1) Voorraad schoenen bekijken");
                    Console.WriteLine("2) Schoenen inkopen");
                    Console.WriteLine("3) Uitloggen");
                    int option = int.Parse(Console.ReadLine());
                    Console.Clear();
                    int result = 0;
                    if (option == 3)
                    {
                        Console.WriteLine("Je bent uitgelogd");
                        programIsrunning = false;
                        username = string.Empty;
                        password = string.Empty;
                        attempts = 0;
                    }
                    else if (option == 2)
                    {


                        Console.WriteLine("Schoenen inkopen");
                        for (int i = 0; i < schoenenlijst.GetLength(0); i++)
                        {
                            for (int j = 0; j < schoenenlijst.GetLength(1); j++)
                            {
                                Console.Write(schoenenlijst[i, j].PadRight(10));
                            }
                            Console.WriteLine();
                        }

                        Console.WriteLine("Welke merk schoenen wil je inkopen ?");
                        Console.WriteLine("1) Nike");
                        Console.WriteLine("2) Adidas");
                        Console.WriteLine("3) Lacoste");
                        int schoenen = int.Parse(Console.ReadLine());
                        Console.Clear();
                        if (schoenen == 1)
                        {
                            Console.WriteLine("Hoeveel schoenen wil je inkopen?");
                            int nike = Convert.ToInt32(Console.ReadLine());
                            if (nike <= 0)
                            {
                                Console.WriteLine("vul een positief getal in");
                                nike = Convert.ToInt32(Console.ReadLine());
                            }
                            else
                            {
                                int nieuweschoenen = Convert.ToInt32(schoenenlijst[0, 5]);
                                int merkschoenen = nike + nieuweschoenen;
                                schoenenlijst[0, 5] = merkschoenen.ToString();
                                Console.WriteLine("nieuwe vooraad is: " + schoenenlijst[0, 5]);
                            }
                        }
                        else if (schoenen == 2)
                        {
                            Console.WriteLine("Hoeveel schoenen wil je inkopen?");
                            int adidas = Convert.ToInt32(Console.ReadLine());
                            if (adidas <= 0)
                            {
                                Console.WriteLine("vul een positief getal in");
                                adidas = Convert.ToInt32(Console.ReadLine());
                            }
                            else
                            {
                                int nieuweschoenen = Convert.ToInt32(schoenenlijst[0, 5]);
                                int merkschoenen = adidas + nieuweschoenen;
                                schoenenlijst[0, 5] = merkschoenen.ToString();
                                Console.WriteLine("nieuwe voorraad is: " + schoenenlijst[0, 5]);
                            }
                        }
                        else if (schoenen == 3)
                        {
                            Console.WriteLine("Hoeveel schoenen wil je inkopen?");
                            int lacoste = Convert.ToInt32(Console.ReadLine());
                            if (lacoste <= 0)
                            {
                                Console.WriteLine("vul een positief getal in");
                                lacoste = Convert.ToInt32(Console.ReadLine());
                            }
                            else
                            {
                                int nieuweschoenen = Convert.ToInt32(schoenenlijst[0, 5]);
                                int merkschoenen = lacoste + nieuweschoenen;
                                schoenenlijst[0, 5] = merkschoenen.ToString();
                                Console.WriteLine("Nieuwe vooraad is: " + schoenenlijst[0, 5]);
                            }
                        }
                        Console.WriteLine("toets enter om weer bij menu te komen");
                        String kiesmenu = Console.ReadLine();
                    }
                    else if (option == 1)
                    {
                        Console.Clear();
                        Console.WriteLine("Vooraad schoenen bekijken");

                        for (int i = 0; i < schoenenlijst.GetLength(0); i++)
                        {
                            for (int j = 0; j < schoenenlijst.GetLength(1); j++)
                            {
                                Console.Write(schoenenlijst[i, j].PadRight(10));
                            }
                            Console.WriteLine();
                        }
                        Console.WriteLine("toets enter om weer bij menu te komen");
                        String kiesmenu = Console.ReadLine();
                    }
                }
            }
            else
            {
                attempts++;
                username = string.Empty;
                password = string.Empty;
                Console.WriteLine("");
                Console.Clear();
                Console.WriteLine("Onjuiste inloggegevens, Je hebt nog " + (3 - attempts) + " pogingen over");
                if (attempts == 3)
                {
                    Environment.Exit(0);
                }
            }
        }
    }
}