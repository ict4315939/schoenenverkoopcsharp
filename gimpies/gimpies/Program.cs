﻿using System;
using System.Drawing;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;
using static System.Collections.Specialized.BitVector32;
using System.Data.SqlClient;
using System.Reflection.PortableExecutable;

public class Exercise16
{
    public static object FormsAuthentication { get; private set; }
    

    public static void Main()
    {
        bool programIsrunning = true;
        int attempts = 0;
        var password = string.Empty;
        var username = string.Empty;
        int nikeschoenen = 40;
        int lacosteschoenen = 60;
        int adidasschoenen = 80;
        bool aut = false;
        string[,] schoenenlijst = new string[3, 7] {
                { "1", "Nike", "Air max", "42", "wit",nikeschoenen.ToString(), "110,99" },
                { "2", "Adidas", "Ozwego", "42", "zwart",adidasschoenen.ToString(), "100,00" },
                { "3", "Lacoste", "t-Clip ", "40", "zwart",lacosteschoenen.ToString(), "120,00" }};
           string connectionString = "Data Source=LAPTOP-Q6JNDF4T\\SQLEXPRESS;Initial Catalog=Gimpies;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
          SqlConnection connection = new SqlConnection(connectionString);

        while (attempts < 3)
        {
            Console.WriteLine("Enter your Username : ");
            string Username = Console.ReadLine();
            Console.WriteLine("Enter your password: ");
            ConsoleKey key;
            do
            {
                var keyInfo = Console.ReadKey(intercept: true);
                key = keyInfo.Key;

                if (key == ConsoleKey.Backspace && password.Length > 0)
                {
                    Console.Write("\b \b");
                    password = password[0..^1];
                }
                else if (!char.IsControl(keyInfo.KeyChar))
                {
                    Console.Write("*");
                    password += keyInfo.KeyChar;
                }
            } while (key != ConsoleKey.Enter);

            SqlCommand cdm = new SqlCommand("SELECT  * FROM users_taak WHERE username = @username AND password = @password", connection);
            cdm.Parameters.AddWithValue("@username", Username);
            cdm.Parameters.AddWithValue("@password", password);
            connection.Open();

            SqlDataReader readers = cdm.ExecuteReader();

            if (readers.HasRows)
            {
                readers.Read();
                Console.WriteLine("");
                Console.WriteLine("Je bent ingelogd");
                programIsrunning = true;
                readers.Close();


                while (programIsrunning == true)
                    {

                        Console.Clear();
                        Console.WriteLine("Maak je keuze:");
                        Console.WriteLine("1) Voorraad schoenen bekijken");
                        Console.WriteLine("2) Schoenen inkopen");
                        Console.WriteLine("3) Uitloggen");
                        int option = int.Parse(Console.ReadLine());
                        Console.Clear();
                        if (option == 3)
                        {
                            Console.WriteLine("Je bent uitgelogd");
                            programIsrunning = false;
                             username = string.Empty;
                             password = string.Empty;
                             attempts = 0;
                        }
                        else if (option == 2)
                        {


                                Console.WriteLine("Schoenen inkopen");
                            SqlCommand comman = new SqlCommand("SELECT DISTINCT Merk FROM schoenenLijst", connection);

                          //  connection.Open();

                            SqlDataReader read = comman.ExecuteReader();

                            while (read.Read())
                            {
                                string brands = read.GetString(0);
                                Console.WriteLine(brands);
                            }

                            read.Close();
                           // connection.Close();
                        Console.WriteLine("Kies de Merk:");
                            string brand = Console.ReadLine();
                            Console.WriteLine("Voeg het aaantal toe:");
                            int stockToAdd = int.Parse(Console.ReadLine());


                             string selectQuery = "SELECT Aantal FROM schoenenLijst WHERE Merk = @Brand";
                            SqlCommand selectCommand = new SqlCommand(selectQuery, connection);
                            selectCommand.Parameters.AddWithValue("@Brand", brand);
                            int existingStock = (int)selectCommand.ExecuteScalar();

                            int newStock = existingStock + stockToAdd;
                            string updateQuery = "UPDATE schoenenLijst SET Aantal = @NewStock WHERE Merk = @Brand";
                            SqlCommand update = new SqlCommand(updateQuery, connection);
                            update.Parameters.AddWithValue("@NewStock", newStock);
                            update.Parameters.AddWithValue("@Brand", brand);
                            update.ExecuteNonQuery();

                              Console.WriteLine("Top het is gelukt!");



                            Console.WriteLine("toets enter om weer bij menu te komen");
                                String kiesmenu = Console.ReadLine();

                        }                                                         
                        else if (option == 1)                                     
                        {                                                         
                            Console.Clear();
                            Console.WriteLine("Vooraad schoenen bekijken");
                           SqlCommand command = new SqlCommand("SELECT * FROM schoenenlijst", connection);
                          SqlDataReader reader = command.ExecuteReader();
                            Console.WriteLine("ID | Type |                   | Merk |                        | Kleur |                  | Prijs | Aantal |");
                            Console.WriteLine("-----------------------------------------------------------------------------------------------------------");
                            while (reader.Read())
                            {
                                Console.WriteLine("{0}-{1}{2}-{3} - {4} - {5}", reader["ID"], reader["Type"], reader["Merk"], reader["Kleur"], reader["Prijs"], reader["Aantal"]);
                            }
                            Console.WriteLine("toets enter om weer bij menu te komen");
                                   String kiesmenu = Console.ReadLine();
                            reader.Close();
                            }
                    }
           

            }
            else
            {
                attempts++;
                username = string.Empty;
                password = string.Empty;
                Console.WriteLine("");
                Console.Clear();
                Console.WriteLine("Onjuiste inloggegevens, Je hebt nog " + (3 - attempts) + " pogingen over");
                if (attempts== 3)
                {
                    Environment.Exit(0);
                }
            }
            connection.Close();
        }
    }
}