﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gimpies_sprint_2
{
    public partial class menuform : Form
    {
        public menuform()
        {
            InitializeComponent();
        }
        private void addusercontrol(UserControl userControl)
        {
            userControl.Dock= DockStyle.Fill;
            panel1.Controls.Clear();
            panel1.Controls.Add(userControl);
            userControl.BringToFront();
        }


        private void button4_Click(object sender, EventArgs e)
        {
            Login Login = new Login();
            Login.Show();
            this.Close();
        }

        private void btn_voorraadschoen_Click(object sender, EventArgs e)
        {
            voorraad_schoenen voorraad_Schoenen = new voorraad_schoenen();
            voorraad_Schoenen.Show();
            this.Close();
        }

        private void btn_schoenenverkopen_Click(object sender, EventArgs e)
        {
            Schoenenverkopen schoenenverkopen = new Schoenenverkopen();
            schoenenverkopen.Show();
            this.Close();
        }

        private void menuform_Load(object sender, EventArgs e)
        {

        }
    }
}
