﻿namespace Gimpies_sprint_2
{
    partial class menuform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_uitloggen = new System.Windows.Forms.Button();
            this.btn_schoenenverkopen = new System.Windows.Forms.Button();
            this.btn_voorraadschoen = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SteelBlue;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1485, 61);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(636, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 37);
            this.label1.TabIndex = 11;
            this.label1.Text = "Gimpies";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.btn_uitloggen);
            this.panel2.Controls.Add(this.btn_schoenenverkopen);
            this.panel2.Controls.Add(this.btn_voorraadschoen);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 61);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1485, 65);
            this.panel2.TabIndex = 1;
            // 
            // btn_uitloggen
            // 
            this.btn_uitloggen.BackColor = System.Drawing.Color.DarkCyan;
            this.btn_uitloggen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_uitloggen.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_uitloggen.ForeColor = System.Drawing.Color.White;
            this.btn_uitloggen.Location = new System.Drawing.Point(552, 5);
            this.btn_uitloggen.Name = "btn_uitloggen";
            this.btn_uitloggen.Size = new System.Drawing.Size(195, 56);
            this.btn_uitloggen.TabIndex = 3;
            this.btn_uitloggen.Text = "Uitloggen";
            this.btn_uitloggen.UseVisualStyleBackColor = false;
            this.btn_uitloggen.Click += new System.EventHandler(this.button4_Click);
            // 
            // btn_schoenenverkopen
            // 
            this.btn_schoenenverkopen.BackColor = System.Drawing.Color.Teal;
            this.btn_schoenenverkopen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_schoenenverkopen.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_schoenenverkopen.ForeColor = System.Drawing.Color.Honeydew;
            this.btn_schoenenverkopen.Location = new System.Drawing.Point(316, 5);
            this.btn_schoenenverkopen.Name = "btn_schoenenverkopen";
            this.btn_schoenenverkopen.Size = new System.Drawing.Size(219, 56);
            this.btn_schoenenverkopen.TabIndex = 2;
            this.btn_schoenenverkopen.Text = "Schoenen verkopen";
            this.btn_schoenenverkopen.UseVisualStyleBackColor = false;
            this.btn_schoenenverkopen.Click += new System.EventHandler(this.btn_schoenenverkopen_Click);
            // 
            // btn_voorraadschoen
            // 
            this.btn_voorraadschoen.BackColor = System.Drawing.Color.DarkCyan;
            this.btn_voorraadschoen.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_voorraadschoen.ForeColor = System.Drawing.Color.White;
            this.btn_voorraadschoen.Location = new System.Drawing.Point(20, 5);
            this.btn_voorraadschoen.Name = "btn_voorraadschoen";
            this.btn_voorraadschoen.Size = new System.Drawing.Size(287, 57);
            this.btn_voorraadschoen.TabIndex = 1;
            this.btn_voorraadschoen.Text = "Vooraad schoenen bekijken";
            this.btn_voorraadschoen.UseVisualStyleBackColor = false;
            this.btn_voorraadschoen.Click += new System.EventHandler(this.btn_voorraadschoen_Click);
            // 
            // menuform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1485, 620);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "menuform";
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.menuform_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private Panel panel2;
        private Button btn_uitloggen;
        private Button btn_schoenenverkopen;
        private Button btn_voorraadschoen;
        private Button btn_home;
        private Label label1;
    }
}