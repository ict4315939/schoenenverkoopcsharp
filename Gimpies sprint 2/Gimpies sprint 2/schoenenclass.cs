﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gimpies_sprint_2
{
    public class schoenen
    {
            public int ID { get; set; }
            public string merk { get; set; }
            public string type { get; set; }
            public string kleur { get; set; }
            public decimal prijs { get; set; }
            public int aantal { get; set; }
    }
}
