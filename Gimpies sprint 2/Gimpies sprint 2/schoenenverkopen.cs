﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Gimpies_sprint_2
{
    public partial class Schoenenverkopen : Form
    {
        int number=0;
        public Schoenenverkopen()
        {
            InitializeComponent();
        }

        private void Schoenenverkopen_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Global.schoen;
        }

        private void btn_terug_Click(object sender, EventArgs e)
        {
            menuform menuform = new menuform();
            menuform.Show();
            this.Close();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            int huidigAantal;
            int verkochtAantal;
            int.TryParse(dataGridView1.Rows[Global.keuze].Cells[5].Value.ToString(), out huidigAantal) ;
           // int.TryParse(schoenaantal.Text, out verkochtAantal);
            if (!int.TryParse(schoenaantal.Text, out verkochtAantal))
            {
                MessageBox.Show("voer een getal in");
                return;
            }
            else if (verkochtAantal<0)
            {

                MessageBox.Show("voer een positief getal in ");
                return;
            }
            int nieuwAantal = huidigAantal - verkochtAantal;
            if (nieuwAantal< 0)
            {
                MessageBox.Show("Het getal die je in hebt gevuld is hoger dan het aantal");
            }
            else
            {
                dataGridView1.Rows[Global.keuze].Cells[5].Value=nieuwAantal;
                dataGridView1.Update();
                dataGridView1.Refresh();
            }
        }
        private void combokeuze_SelectedIndexChanged(object sender, EventArgs e)
        {

            combokeuze.DataSource = Global.schoen;
            combokeuze.DisplayMember = "merk";
            Global.keuze=combokeuze.SelectedIndex;
        }

        private void schoenaantal_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
