﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gimpies_sprint_2
{
    public partial class schoenenbeheer : Form
    {
        public schoenenbeheer()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void schoenenbeheer_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Global.schoen;
        }

        private void toevoegen_Click(object sender, EventArgs e)
        {
            int ID;
            int Aantal=0;
            decimal prijs;
            if (!int.TryParse(id.Text, out ID))
            {
                MessageBox.Show("Ongeldige invoer voor ID");
                return;
            }
            else if (Global.schoen.Any(s => s.ID == ID))
            {
                MessageBox.Show("Er bestaat al een schoen met dit ID");
                return;
            }
            string Merk = merk.Text;
            string Type =txt_Type.Text;
            string Kleur = kleur.Text;
            if (!decimal.TryParse(telefoon.Text, out prijs))
            {
                MessageBox.Show("Geef een gatal op");
                return;
            }

            schoenen addshoes = new schoenen
            {
                ID = ID,
                merk = Merk,
                type = Type,
                kleur = Kleur,
                prijs = prijs,
                aantal = Aantal
            };

            Global.schoen.Add(addshoes);

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Global.schoen;
        }

        private void btn_terug_Click(object sender, EventArgs e)
        {
            menumanger menumanger = new menumanger();
            menumanger.Show();
            this.Close();
        }


        private void verwijderen_Click(object sender, EventArgs e)
        {
            DialogResult Delete;
            Delete = MessageBox.Show("wil je het verwijderen", "Exit", MessageBoxButtons.YesNo);
            if (Delete == DialogResult.Yes)
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    schoenen deleteShoe = row.DataBoundItem as schoenen;
                    Global.schoen.Remove(deleteShoe);
                    dataGridView1.DataSource = null;
                    dataGridView1.DataSource = Global.schoen;
                }
            }
        }

        private void aanpassen_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index;
                int id;
                string Merk = merk.Text;
                string Type= txt_Type.Text;
                string Kleur = kleur.Text;
                decimal Prijs;
                int Aantal;

                if (int.TryParse(dataGridView1.SelectedRows[0].Index.ToString(), out index) &&
                    int.TryParse(dataGridView1.Rows[index].Cells["ID"].Value.ToString(), out id) &&
                    decimal.TryParse(telefoon.Text, out Prijs))
                {
                    var shoe = Global.schoen.FirstOrDefault(s => s.ID == id);
                    if (shoe != null)
                    {                 
                        shoe.ID= id;  
                        shoe.merk = Merk;  
                        shoe.type= Type;
                        shoe.kleur = Kleur; 
                        shoe.prijs = Prijs; 
                    }
                    dataGridView1.DataSource = null;
                    dataGridView1.DataSource = Global.schoen;
                }
                else
                {              
                        MessageBox.Show("Een of meer velden hebben een ongeldige waarde.");  
                }
            }
            else
            {
                MessageBox.Show("Geen rij geselecteerd.");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
            id.Text = row.Cells[0].Value.ToString();
            merk.Text = row.Cells[1].Value.ToString();
            txt_Type.Text = row.Cells[2].Value.ToString();
            kleur.Text = row.Cells[3].Value.ToString();
            aantal.Text = row.Cells[5].Value.ToString();
            telefoon.Text = row.Cells[4].Value.ToString();
        }
    }
}
