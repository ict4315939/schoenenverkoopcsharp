﻿namespace Gimpies_sprint_2
{
    partial class schoenenbeheer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.merk = new System.Windows.Forms.TextBox();
            this.aantal = new System.Windows.Forms.TextBox();
            this.telefoon = new System.Windows.Forms.TextBox();
            this.kleur = new System.Windows.Forms.TextBox();
            this.toevoegen = new System.Windows.Forms.Button();
            this.aanpassen = new System.Windows.Forms.Button();
            this.verwijderen = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.id = new System.Windows.Forms.TextBox();
            this.btn_terug = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_Type = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.DimGray;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(135, 322);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(744, 169);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // merk
            // 
            this.merk.Location = new System.Drawing.Point(624, 74);
            this.merk.Name = "merk";
            this.merk.Size = new System.Drawing.Size(150, 31);
            this.merk.TabIndex = 3;
            // 
            // aantal
            // 
            this.aantal.Location = new System.Drawing.Point(624, 208);
            this.aantal.Name = "aantal";
            this.aantal.Size = new System.Drawing.Size(150, 31);
            this.aantal.TabIndex = 4;
            // 
            // telefoon
            // 
            this.telefoon.Location = new System.Drawing.Point(624, 174);
            this.telefoon.Name = "telefoon";
            this.telefoon.Size = new System.Drawing.Size(150, 31);
            this.telefoon.TabIndex = 5;
            // 
            // kleur
            // 
            this.kleur.Location = new System.Drawing.Point(624, 139);
            this.kleur.Name = "kleur";
            this.kleur.Size = new System.Drawing.Size(150, 31);
            this.kleur.TabIndex = 6;
            // 
            // toevoegen
            // 
            this.toevoegen.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.toevoegen.Location = new System.Drawing.Point(231, 255);
            this.toevoegen.Name = "toevoegen";
            this.toevoegen.Size = new System.Drawing.Size(144, 34);
            this.toevoegen.TabIndex = 7;
            this.toevoegen.Text = "Toevoegen";
            this.toevoegen.UseVisualStyleBackColor = true;
            this.toevoegen.Click += new System.EventHandler(this.toevoegen_Click);
            // 
            // aanpassen
            // 
            this.aanpassen.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.aanpassen.Location = new System.Drawing.Point(402, 255);
            this.aanpassen.Name = "aanpassen";
            this.aanpassen.Size = new System.Drawing.Size(139, 34);
            this.aanpassen.TabIndex = 8;
            this.aanpassen.Text = "Aanpassen";
            this.aanpassen.UseVisualStyleBackColor = true;
            this.aanpassen.Click += new System.EventHandler(this.aanpassen_Click);
            // 
            // verwijderen
            // 
            this.verwijderen.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.verwijderen.Location = new System.Drawing.Point(575, 255);
            this.verwijderen.Name = "verwijderen";
            this.verwijderen.Size = new System.Drawing.Size(140, 34);
            this.verwijderen.TabIndex = 9;
            this.verwijderen.Text = "Verwijderen";
            this.verwijderen.UseVisualStyleBackColor = true;
            this.verwijderen.Click += new System.EventHandler(this.verwijderen_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(527, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 23);
            this.label1.TabIndex = 10;
            this.label1.Text = "Merk:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(527, 213);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 23);
            this.label2.TabIndex = 11;
            this.label2.Text = "Aantal:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(531, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 23);
            this.label3.TabIndex = 12;
            this.label3.Text = "Prijs:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(527, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 23);
            this.label4.TabIndex = 13;
            this.label4.Text = "Kleur:";
            // 
            // id
            // 
            this.id.Location = new System.Drawing.Point(624, 39);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(150, 31);
            this.id.TabIndex = 14;
            // 
            // btn_terug
            // 
            this.btn_terug.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_terug.Location = new System.Drawing.Point(59, -1);
            this.btn_terug.Name = "btn_terug";
            this.btn_terug.Size = new System.Drawing.Size(112, 34);
            this.btn_terug.TabIndex = 15;
            this.btn_terug.Text = "< Terug";
            this.btn_terug.UseVisualStyleBackColor = true;
            this.btn_terug.Click += new System.EventHandler(this.btn_terug_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(540, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 23);
            this.label5.TabIndex = 16;
            this.label5.Text = "ID:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(527, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 23);
            this.label6.TabIndex = 17;
            this.label6.Text = "Type:";
            // 
            // txt_Type
            // 
            this.txt_Type.Location = new System.Drawing.Point(624, 108);
            this.txt_Type.Name = "txt_Type";
            this.txt_Type.Size = new System.Drawing.Size(150, 31);
            this.txt_Type.TabIndex = 18;
            // 
            // schoenenbeheer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1094, 574);
            this.Controls.Add(this.txt_Type);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_terug);
            this.Controls.Add(this.id);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.verwijderen);
            this.Controls.Add(this.aanpassen);
            this.Controls.Add(this.toevoegen);
            this.Controls.Add(this.kleur);
            this.Controls.Add(this.telefoon);
            this.Controls.Add(this.aantal);
            this.Controls.Add(this.merk);
            this.Controls.Add(this.dataGridView1);
            this.Name = "schoenenbeheer";
            this.Text = "schoenenbeheer";
            this.Load += new System.EventHandler(this.schoenenbeheer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataGridView dataGridView1;
        private TextBox merk;
        private TextBox aantal;
        private TextBox telefoon;
        private TextBox kleur;
        private Button toevoegen;
        private Button aanpassen;
        private Button verwijderen;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private TextBox id;
        private Button btn_terug;
        private Label label5;
        private Label label6;
        private TextBox txt_Type;
    }
}