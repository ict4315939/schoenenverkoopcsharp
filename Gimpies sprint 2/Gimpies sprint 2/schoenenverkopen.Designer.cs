﻿namespace Gimpies_sprint_2
{
    partial class Schoenenverkopen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_terug = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.combokeuze = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.schoenaantal = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(169, 282);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(744, 169);
            this.dataGridView1.TabIndex = 1;
            // 
            // btn_terug
            // 
            this.btn_terug.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_terug.Location = new System.Drawing.Point(121, 30);
            this.btn_terug.Name = "btn_terug";
            this.btn_terug.Size = new System.Drawing.Size(112, 34);
            this.btn_terug.TabIndex = 2;
            this.btn_terug.Text = "< Terug";
            this.btn_terug.UseVisualStyleBackColor = true;
            this.btn_terug.Click += new System.EventHandler(this.btn_terug_Click);
            // 
            // btn_add
            // 
            this.btn_add.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_add.Location = new System.Drawing.Point(710, 158);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(149, 34);
            this.btn_add.TabIndex = 3;
            this.btn_add.Text = "Verkopen";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // combokeuze
            // 
            this.combokeuze.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.combokeuze.FormattingEnabled = true;
            this.combokeuze.Items.AddRange(new object[] {
            "Nike",
            "Puma",
            "Adidas",
            "Lacoste"});
            this.combokeuze.Location = new System.Drawing.Point(51, 157);
            this.combokeuze.Name = "combokeuze";
            this.combokeuze.Size = new System.Drawing.Size(182, 31);
            this.combokeuze.TabIndex = 4;
            this.combokeuze.SelectedIndexChanged += new System.EventHandler(this.combokeuze_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(10, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 23);
            this.label1.TabIndex = 5;
            this.label1.Text = "Maak je schoenkeuze:";
            // 
            // schoenaantal
            // 
            this.schoenaantal.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.schoenaantal.Location = new System.Drawing.Point(305, 157);
            this.schoenaantal.Name = "schoenaantal";
            this.schoenaantal.Size = new System.Drawing.Size(150, 31);
            this.schoenaantal.TabIndex = 6;
            this.schoenaantal.TextChanged += new System.EventHandler(this.schoenaantal_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(279, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(345, 23);
            this.label2.TabIndex = 7;
            this.label2.Text = "Hoeveel schoenen wil je verkopen:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // Schoenenverkopen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(1069, 572);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.schoenaantal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.combokeuze);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.btn_terug);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Schoenenverkopen";
            this.Text = "schoenenverkopen";
            this.Load += new System.EventHandler(this.Schoenenverkopen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataGridView dataGridView1;
        private Button btn_terug;
        private Button btn_add;
        private ComboBox combokeuze;
        private Label label1;
        private TextBox schoenaantal;
        private Label label2;
    }
}