﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gimpies_sprint_2
{
    public partial class vooraadschoenenadmin : Form
    {
        public vooraadschoenenadmin()
        {
            InitializeComponent();
        }

        private void vooraadschoenenadmin_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Global.schoen;

        }

        private void btn_terug_Click(object sender, EventArgs e)
        {
            menumanger menumanger = new menumanger();
            menumanger.Show();
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.DataSource = Global.schoen;
        }
    }
}
