﻿namespace sprint3_gimpies
{
    partial class Medewerkersbeheer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_toevoegen = new System.Windows.Forms.Button();
            this.btm_aanpassen = new System.Windows.Forms.Button();
            this.btn_verwijderen = new System.Windows.Forms.Button();
            this.txt_naam = new System.Windows.Forms.TextBox();
            this.txt_wachtwoord = new System.Windows.Forms.TextBox();
            this.txt_taak = new System.Windows.Forms.TextBox();
            this.btn_terug = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView2
            // 
            this.dataGridView2.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(179, 304);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 62;
            this.dataGridView2.RowTemplate.Height = 28;
            this.dataGridView2.Size = new System.Drawing.Size(698, 241);
            this.dataGridView2.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(174, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(157, 29);
            this.label6.TabIndex = 7;
            this.label6.Text = "Gebruiksnaam:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(174, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 29);
            this.label1.TabIndex = 8;
            this.label1.Text = "Wachtwoord:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(174, 156);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 29);
            this.label2.TabIndex = 9;
            this.label2.Text = "Taak:";
            // 
            // btn_toevoegen
            // 
            this.btn_toevoegen.BackColor = System.Drawing.Color.Azure;
            this.btn_toevoegen.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_toevoegen.Location = new System.Drawing.Point(134, 216);
            this.btn_toevoegen.Name = "btn_toevoegen";
            this.btn_toevoegen.Size = new System.Drawing.Size(156, 55);
            this.btn_toevoegen.TabIndex = 17;
            this.btn_toevoegen.Text = "Toevoegen";
            this.btn_toevoegen.UseVisualStyleBackColor = false;
            this.btn_toevoegen.Click += new System.EventHandler(this.btn_toevoegen_Click);
            // 
            // btm_aanpassen
            // 
            this.btm_aanpassen.BackColor = System.Drawing.Color.Azure;
            this.btm_aanpassen.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btm_aanpassen.Location = new System.Drawing.Point(326, 216);
            this.btm_aanpassen.Name = "btm_aanpassen";
            this.btm_aanpassen.Size = new System.Drawing.Size(156, 55);
            this.btm_aanpassen.TabIndex = 19;
            this.btm_aanpassen.Text = "Aanpassen";
            this.btm_aanpassen.UseVisualStyleBackColor = false;
            this.btm_aanpassen.Click += new System.EventHandler(this.btm_aanpassen_Click);
            // 
            // btn_verwijderen
            // 
            this.btn_verwijderen.BackColor = System.Drawing.Color.Azure;
            this.btn_verwijderen.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_verwijderen.Location = new System.Drawing.Point(511, 216);
            this.btn_verwijderen.Name = "btn_verwijderen";
            this.btn_verwijderen.Size = new System.Drawing.Size(156, 55);
            this.btn_verwijderen.TabIndex = 20;
            this.btn_verwijderen.Text = "Verwijderen";
            this.btn_verwijderen.UseVisualStyleBackColor = false;
            this.btn_verwijderen.Click += new System.EventHandler(this.btn_verwijderen_Click);
            // 
            // txt_naam
            // 
            this.txt_naam.Location = new System.Drawing.Point(343, 67);
            this.txt_naam.Name = "txt_naam";
            this.txt_naam.Size = new System.Drawing.Size(217, 26);
            this.txt_naam.TabIndex = 21;
            // 
            // txt_wachtwoord
            // 
            this.txt_wachtwoord.Location = new System.Drawing.Point(343, 115);
            this.txt_wachtwoord.Name = "txt_wachtwoord";
            this.txt_wachtwoord.Size = new System.Drawing.Size(217, 26);
            this.txt_wachtwoord.TabIndex = 22;
            // 
            // txt_taak
            // 
            this.txt_taak.Location = new System.Drawing.Point(342, 160);
            this.txt_taak.Name = "txt_taak";
            this.txt_taak.Size = new System.Drawing.Size(217, 26);
            this.txt_taak.TabIndex = 23;
            // 
            // btn_terug
            // 
            this.btn_terug.BackColor = System.Drawing.Color.Azure;
            this.btn_terug.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_terug.Location = new System.Drawing.Point(35, 17);
            this.btn_terug.Name = "btn_terug";
            this.btn_terug.Size = new System.Drawing.Size(121, 35);
            this.btn_terug.TabIndex = 24;
            this.btn_terug.Text = "< Terug";
            this.btn_terug.UseVisualStyleBackColor = false;
            this.btn_terug.Click += new System.EventHandler(this.btn_terug_Click);
            // 
            // Medewerkersbeheer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SeaGreen;
            this.ClientSize = new System.Drawing.Size(1093, 609);
            this.Controls.Add(this.btn_terug);
            this.Controls.Add(this.txt_taak);
            this.Controls.Add(this.txt_wachtwoord);
            this.Controls.Add(this.txt_naam);
            this.Controls.Add(this.btn_verwijderen);
            this.Controls.Add(this.btm_aanpassen);
            this.Controls.Add(this.btn_toevoegen);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dataGridView2);
            this.Name = "Medewerkersbeheer";
            this.Text = "Medewerkersbeheer";
            this.Load += new System.EventHandler(this.Medewerkersbeheer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_toevoegen;
        private System.Windows.Forms.Button btm_aanpassen;
        private System.Windows.Forms.Button btn_verwijderen;
        private System.Windows.Forms.TextBox txt_naam;
        private System.Windows.Forms.TextBox txt_wachtwoord;
        private System.Windows.Forms.TextBox txt_taak;
        private System.Windows.Forms.Button btn_terug;
    }
}