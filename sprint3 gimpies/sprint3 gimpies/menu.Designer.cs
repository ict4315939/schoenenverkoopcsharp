﻿namespace sprint3_gimpies
{
    partial class menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_vorraad = new System.Windows.Forms.Button();
            this.btn_uitloggen = new System.Windows.Forms.Button();
            this.btn_verkopen = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.CadetBlue;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1307, 138);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("ROG Fonts", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(550, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome";
            // 
            // btn_vorraad
            // 
            this.btn_vorraad.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btn_vorraad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_vorraad.Location = new System.Drawing.Point(144, 144);
            this.btn_vorraad.Name = "btn_vorraad";
            this.btn_vorraad.Size = new System.Drawing.Size(256, 84);
            this.btn_vorraad.TabIndex = 2;
            this.btn_vorraad.Text = "Voorraad Bekijken";
            this.btn_vorraad.UseVisualStyleBackColor = false;
            this.btn_vorraad.Click += new System.EventHandler(this.btn_vorraad_Click);
            // 
            // btn_uitloggen
            // 
            this.btn_uitloggen.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btn_uitloggen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_uitloggen.Location = new System.Drawing.Point(938, 144);
            this.btn_uitloggen.Name = "btn_uitloggen";
            this.btn_uitloggen.Size = new System.Drawing.Size(256, 84);
            this.btn_uitloggen.TabIndex = 5;
            this.btn_uitloggen.Text = "Uitloggen";
            this.btn_uitloggen.UseVisualStyleBackColor = false;
            this.btn_uitloggen.Click += new System.EventHandler(this.btn_uitloggen_Click);
            // 
            // btn_verkopen
            // 
            this.btn_verkopen.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btn_verkopen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_verkopen.Location = new System.Drawing.Point(547, 144);
            this.btn_verkopen.Name = "btn_verkopen";
            this.btn_verkopen.Size = new System.Drawing.Size(256, 84);
            this.btn_verkopen.TabIndex = 6;
            this.btn_verkopen.Text = "Verkoop Schoenen";
            this.btn_verkopen.UseVisualStyleBackColor = false;
            this.btn_verkopen.Click += new System.EventHandler(this.btn_verkopen_Click);
            // 
            // menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1307, 716);
            this.Controls.Add(this.btn_verkopen);
            this.Controls.Add(this.btn_uitloggen);
            this.Controls.Add(this.btn_vorraad);
            this.Controls.Add(this.panel1);
            this.Name = "menu";
            this.Text = "menu";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_vorraad;
        private System.Windows.Forms.Button btn_uitloggen;
        private System.Windows.Forms.Button btn_verkopen;
    }
}