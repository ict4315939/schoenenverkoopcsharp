﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sprint3_gimpies
{
    public partial class menumanger : Form
    {
        public menumanger()
        {
            InitializeComponent();
        }

        private void btn_uitloggen_Click(object sender, EventArgs e)
        {
            login login = new login();
            login.Show();
            this.Close();
        }

        private void btn_beheer_Click(object sender, EventArgs e)
        {
            Schoenenbeheer schoenenbeheer = new Schoenenbeheer();
            schoenenbeheer.Show();
            this.Close();
        }

        private void btn_mederwerkers_Click(object sender, EventArgs e)
        {
            Medewerkersbeheer beheer = new Medewerkersbeheer();
            beheer.Show();
            this.Close();
        }
    }
}
