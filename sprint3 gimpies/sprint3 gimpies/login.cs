﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Collections;

namespace sprint3_gimpies
{
    public partial class login : Form
    {
        int attempts = 3;
        public login()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data Source=LAPTOP-Q6JNDF4T\SQLEXPRESS;Initial Catalog=Gimpies;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            txt_username.Clear();
            txt_password.Clear();
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            String username,password;
            username = txt_username.Text;
            password = txt_password.Text;


                     string querry = "SELECT  * FROM users_taak WHERE username = @username AND password = @password"; 
                    SqlCommand command = new SqlCommand(querry, conn);
                    command.Parameters.AddWithValue("@username", username);
                    command.Parameters.AddWithValue("@password", password);
                    conn.Open();
                    SqlDataReader reader = command.ExecuteReader();
                     if (reader.HasRows)
                     {
                        reader.Read();
                        if (Convert.ToString(reader["Taak"]).Trim() == "admin")
                        {
                            menumanger menumanger = new menumanger();
                            menumanger.Show();
                            this.Hide();
                        }
                        else if (Convert.ToString(reader["Taak"]).Trim() == "verkoper")
                        {
                            menu menu = new menu();
                            menu.Show();
                            this.Hide();
                        }

                     }
                    else
                    {
                            attempts--;
                            if (attempts > 0)
                            {
                                MessageBox.Show($"wachtwoord of gebruikernaam klopt niet je hebt {attempts} pogingen over.");
                                txt_username.Clear();
                                txt_password.Clear();
                            }
                           else {
                             MessageBox.Show("je had 3 keer fout, probeer later.");
                            this.Close();
                            }
                    }
               conn.Close();
        }

        private void login_Load(object sender, EventArgs e)
        {

        }
    }
}
