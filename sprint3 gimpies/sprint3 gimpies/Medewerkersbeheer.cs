﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sprint3_gimpies
{
    public partial class Medewerkersbeheer : Form
    {
        public Medewerkersbeheer()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=LAPTOP-Q6JNDF4T\\SQLEXPRESS;Initial Catalog=Gimpies;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");


        private void Medewerkersbeheer_Load(object sender, EventArgs e)
        {

            SqlCommand cmd = new SqlCommand("SELECT * FROM users_taak", conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            dataGridView2.DataSource = dt;
            dataGridView2.Columns[0].Visible = false;
            dataGridView2.Columns["ID"].Visible = false;
        }

        private void btn_toevoegen_Click(object sender, EventArgs e)
        {
            conn.Open();
            string insertQuery = "INSERT INTO users_taak (username,password,Taak) VALUES (@username,@password,@Taak)";
            SqlCommand cmd = new SqlCommand(insertQuery, conn);
            cmd.Parameters.AddWithValue("@username", txt_naam.Text);
            cmd.Parameters.AddWithValue("@password", txt_wachtwoord.Text);
            cmd.Parameters.AddWithValue("@Taak", txt_taak.Text);
            cmd.ExecuteNonQuery();
            conn.Close();
            txt_wachtwoord.Text = "";
            txt_naam.Text = "";
            txt_taak.Text = "";
            MessageBox.Show("medewerker is toegevoegd");
            SqlCommand cmda = new SqlCommand("SELECT * FROM users_taak", conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmda);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            dataGridView2.DataSource = dt;
        }

        private void btn_verwijderen_Click(object sender, EventArgs e)
        {
            DataGridViewRow selectedRow = dataGridView2.SelectedRows[0];
            int id = Convert.ToInt32(selectedRow.Cells["ID"].Value);
            string query = "DELETE FROM users_taak WHERE ID = @id";
            if (id != 1)
            {
                 SqlCommand command = new SqlCommand(query, conn);
                 command.Parameters.AddWithValue("@ID", id);
                 conn.Open();
                 command.ExecuteNonQuery();
                conn.Close();            
                dataGridView2.Rows.Remove(selectedRow);
            }
            else
            {
                MessageBox.Show("Deze gebruiker kan je niet verwijderen.");
            }
        }

        private void btn_terug_Click(object sender, EventArgs e)
        {
            menumanger menumanger = new menumanger();
            menumanger.Show();
            this.Close();
        }

        private void btm_aanpassen_Click(object sender, EventArgs e)
        {
            if (txt_naam.Text == "" || txt_wachtwoord.Text == "" || txt_taak.Text == "")
            {
                MessageBox.Show("Vul alsjeblieft de velden in");
            }
            else
            {
                conn.Open();
                String quer = "UPDATE users_taak set username='" + txt_naam.Text + "',password='" + txt_wachtwoord.Text + "',Taak='" + txt_taak.Text + "' Where ID='" + dataGridView2.CurrentRow.Cells[0].Value + "'";
                SqlCommand cmds = new SqlCommand(quer, conn);
                cmds.ExecuteNonQuery();
                MessageBox.Show("Medewerker is aangepast");
                SqlCommand cmda = new SqlCommand("SELECT * FROM users_taak", conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmda);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dataGridView2.DataSource = dt;
                conn.Close();
            }
        }
    }
}
