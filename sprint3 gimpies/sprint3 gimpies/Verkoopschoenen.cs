﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace sprint3_gimpies
{
    public partial class Verkoopschoenen : Form
    {
        public Verkoopschoenen()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=LAPTOP-Q6JNDF4T\\SQLEXPRESS;Initial Catalog=Gimpies;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");


        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Verkoopschoenen_Load(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM schoenenlijst", conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns["ID"].Visible = false;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView1.Rows.Count)
            {
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                int aantal = (int)row.Cells["Aantal"].Value;
                txtbox_aantal.Text = aantal.ToString();
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Selecteer een rij.");
                return;
            }
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dataGridView1.SelectedRows[0];
                int id = (int)row.Cells["ID"].Value;
                int aantal = (int)row.Cells["Aantal"].Value;
                int inkoopAantal = int.Parse(txtbox_aantal.Text);
                int nieuweAantal = aantal - inkoopAantal;
                if (inkoopAantal > aantal)
                {
                    MessageBox.Show("Het getal die je in hebt gevuld is hoger dan het aantal");
                    return;
                }
                else if (inkoopAantal < 0)
                {

                    MessageBox.Show("voer een positief getal in ");
                    return;
                }

                string update = $"UPDATE schoenenLijst SET Aantal = {nieuweAantal} WHERE ID = {id}";
                SqlCommand cmd = new SqlCommand(update, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                row.Cells["Aantal"].Value = nieuweAantal;
                txtbox_aantal.Text = "";
            }
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            menu menu = new menu();
            menu.Show();
            this.Close();
        }
    }
}
