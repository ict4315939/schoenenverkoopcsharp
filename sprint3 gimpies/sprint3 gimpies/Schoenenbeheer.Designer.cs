﻿namespace sprint3_gimpies
{
    partial class Schoenenbeheer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_terug = new System.Windows.Forms.Button();
            this.txt_merk = new System.Windows.Forms.TextBox();
            this.txt_type = new System.Windows.Forms.TextBox();
            this.txt_maat = new System.Windows.Forms.TextBox();
            this.txt_kleur = new System.Windows.Forms.TextBox();
            this.txt_prijs = new System.Windows.Forms.TextBox();
            this.btn_toevoegen = new System.Windows.Forms.Button();
            this.btn_verwijderen = new System.Windows.Forms.Button();
            this.btm_aanpassen = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(39, 418);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(1120, 324);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(161, 148);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "Type:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(161, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 29);
            this.label3.TabIndex = 3;
            this.label3.Text = "Maat:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(161, 267);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 29);
            this.label4.TabIndex = 4;
            this.label4.Text = "Kleur:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(161, 323);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 29);
            this.label5.TabIndex = 5;
            this.label5.Text = "Prijs:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(161, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 29);
            this.label6.TabIndex = 6;
            this.label6.Text = "Merk:";
            // 
            // btn_terug
            // 
            this.btn_terug.BackColor = System.Drawing.Color.Azure;
            this.btn_terug.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_terug.Location = new System.Drawing.Point(39, 32);
            this.btn_terug.Name = "btn_terug";
            this.btn_terug.Size = new System.Drawing.Size(150, 43);
            this.btn_terug.TabIndex = 7;
            this.btn_terug.Text = "< Terug";
            this.btn_terug.UseVisualStyleBackColor = false;
            this.btn_terug.Click += new System.EventHandler(this.btn_terug_Click);
            // 
            // txt_merk
            // 
            this.txt_merk.Location = new System.Drawing.Point(257, 92);
            this.txt_merk.Name = "txt_merk";
            this.txt_merk.Size = new System.Drawing.Size(217, 26);
            this.txt_merk.TabIndex = 8;
            // 
            // txt_type
            // 
            this.txt_type.Location = new System.Drawing.Point(257, 152);
            this.txt_type.Name = "txt_type";
            this.txt_type.Size = new System.Drawing.Size(217, 26);
            this.txt_type.TabIndex = 9;
            // 
            // txt_maat
            // 
            this.txt_maat.Location = new System.Drawing.Point(257, 208);
            this.txt_maat.Name = "txt_maat";
            this.txt_maat.Size = new System.Drawing.Size(217, 26);
            this.txt_maat.TabIndex = 10;
            // 
            // txt_kleur
            // 
            this.txt_kleur.Location = new System.Drawing.Point(257, 270);
            this.txt_kleur.Name = "txt_kleur";
            this.txt_kleur.Size = new System.Drawing.Size(217, 26);
            this.txt_kleur.TabIndex = 11;
            // 
            // txt_prijs
            // 
            this.txt_prijs.Location = new System.Drawing.Point(257, 329);
            this.txt_prijs.Name = "txt_prijs";
            this.txt_prijs.Size = new System.Drawing.Size(217, 26);
            this.txt_prijs.TabIndex = 12;
            // 
            // btn_toevoegen
            // 
            this.btn_toevoegen.BackColor = System.Drawing.Color.Azure;
            this.btn_toevoegen.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_toevoegen.Location = new System.Drawing.Point(576, 323);
            this.btn_toevoegen.Name = "btn_toevoegen";
            this.btn_toevoegen.Size = new System.Drawing.Size(156, 55);
            this.btn_toevoegen.TabIndex = 16;
            this.btn_toevoegen.Text = "Toevoegen";
            this.btn_toevoegen.UseVisualStyleBackColor = false;
            this.btn_toevoegen.Click += new System.EventHandler(this.btn_toevoegen_Click);
            // 
            // btn_verwijderen
            // 
            this.btn_verwijderen.BackColor = System.Drawing.Color.Azure;
            this.btn_verwijderen.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_verwijderen.Location = new System.Drawing.Point(959, 324);
            this.btn_verwijderen.Name = "btn_verwijderen";
            this.btn_verwijderen.Size = new System.Drawing.Size(156, 55);
            this.btn_verwijderen.TabIndex = 17;
            this.btn_verwijderen.Text = "Verwijderen";
            this.btn_verwijderen.UseVisualStyleBackColor = false;
            this.btn_verwijderen.Click += new System.EventHandler(this.btn_verwijderen_Click);
            // 
            // btm_aanpassen
            // 
            this.btm_aanpassen.BackColor = System.Drawing.Color.Azure;
            this.btm_aanpassen.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btm_aanpassen.Location = new System.Drawing.Point(763, 325);
            this.btm_aanpassen.Name = "btm_aanpassen";
            this.btm_aanpassen.Size = new System.Drawing.Size(156, 55);
            this.btm_aanpassen.TabIndex = 18;
            this.btm_aanpassen.Text = "Aanpassen";
            this.btm_aanpassen.UseVisualStyleBackColor = false;
            this.btm_aanpassen.Click += new System.EventHandler(this.btm_aanpassen_Click);
            // 
            // Schoenenbeheer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(1272, 750);
            this.Controls.Add(this.btm_aanpassen);
            this.Controls.Add(this.btn_verwijderen);
            this.Controls.Add(this.btn_toevoegen);
            this.Controls.Add(this.txt_prijs);
            this.Controls.Add(this.txt_kleur);
            this.Controls.Add(this.txt_maat);
            this.Controls.Add(this.txt_type);
            this.Controls.Add(this.txt_merk);
            this.Controls.Add(this.btn_terug);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Schoenenbeheer";
            this.Text = "Schoenenbeheer";
            this.Load += new System.EventHandler(this.Schoenenbeheer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_terug;
        private System.Windows.Forms.TextBox txt_merk;
        private System.Windows.Forms.TextBox txt_type;
        private System.Windows.Forms.TextBox txt_maat;
        private System.Windows.Forms.TextBox txt_kleur;
        private System.Windows.Forms.TextBox txt_prijs;
        private System.Windows.Forms.Button btn_toevoegen;
        private System.Windows.Forms.Button btn_verwijderen;
        private System.Windows.Forms.Button btm_aanpassen;
    }
}