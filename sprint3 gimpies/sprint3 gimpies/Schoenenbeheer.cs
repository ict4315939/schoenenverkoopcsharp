﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace sprint3_gimpies
{
    public partial class Schoenenbeheer : Form

    {
        public Schoenenbeheer()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=LAPTOP-Q6JNDF4T\\SQLEXPRESS;Initial Catalog=Gimpies;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        decimal prijs;
        int maat;
        private void btn_terug_Click(object sender, EventArgs e)
        {
            menumanger menumanger = new menumanger();
            menumanger.Show();
            this.Close();
        }
        private void Schoenenbeheer_Load(object sender, EventArgs e)
        {

            SqlCommand cmd = new SqlCommand("SELECT * FROM schoenenlijst", conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns["ID"].Visible = false;
        }
        private void btn_toevoegen_Click(object sender, EventArgs e)
        {
            int add = 0;
            conn.Open();
            string insertQuery = "INSERT INTO schoenenLijst (Type, Merk,Maat,Kleur, Prijs,Aantal) VALUES (@type, @merk,@maat,@kleur, @prijs,@aantal)";
            SqlCommand cmd = new SqlCommand(insertQuery, conn);
            cmd.Parameters.AddWithValue("@type", txt_type.Text);
            cmd.Parameters.AddWithValue("@merk", txt_merk.Text);
            cmd.Parameters.AddWithValue("@kleur", txt_kleur.Text);
            if (!decimal.TryParse(txt_prijs.Text, out prijs))
            {
                MessageBox.Show("Geef een getal op voor de prijs");
                conn.Close();
                txt_prijs.Text = "";               
                return;
            }
            else if (!int.TryParse(txt_maat.Text, out maat))
            {
                MessageBox.Show("Geef een getal op voor de maat");
                conn.Close();
                txt_maat.Text = "";
                return;
            }

            cmd.Parameters.AddWithValue("@prijs", decimal.Parse(txt_prijs.Text));
            cmd.Parameters.AddWithValue("@maat", int.Parse(txt_maat.Text));


            cmd.Parameters.AddWithValue("@aantal", add);
            cmd.ExecuteNonQuery();
            conn.Close();
            txt_type.Text = "";
            txt_merk.Text = "";
            txt_maat.Text = "";
            txt_kleur.Text = "";
            txt_prijs.Text = "";
            MessageBox.Show("schoen is toegevoegd");
            SqlCommand cmda = new SqlCommand("SELECT * FROM schoenenlijst", conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmda);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void btn_verwijderen_Click(object sender, EventArgs e)
        {
            DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];
            int id = Convert.ToInt32(selectedRow.Cells["ID"].Value);
            conn.Open();
            string query = "DELETE FROM schoenenLijst WHERE ID = @id";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@ID", id);
                command.ExecuteNonQuery();
            conn.Close();
            dataGridView1.Rows.Remove(selectedRow);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btm_aanpassen_Click(object sender, EventArgs e)
        {
            if (txt_maat.Text == "" || txt_merk.Text == "" || txt_prijs.Text == "")
            {
                MessageBox.Show("Vul alsjeblieft de velden in");
            }
            else if (!decimal.TryParse(txt_prijs.Text, out prijs))
            {
                MessageBox.Show("Geef een getal op voor de prijs");
                conn.Close();
                txt_prijs.Text = "";
                return;
            }
            else if (!int.TryParse(txt_maat.Text, out maat))
            {
                MessageBox.Show("Geef een getal op voor de maat");
                conn.Close();
                txt_maat.Text = "";
                return;
            }
            else
            {
                 conn.Open();
                String update = $"UPDATE schoenenLijst set Merk='{txt_merk.Text}',Type='{txt_type.Text}', Maat='{txt_maat.Text}',Kleur='" + txt_kleur.Text + "',Prijs='" + txt_prijs.Text + "'Where ID='" + dataGridView1.CurrentRow.Cells[0].Value + "'";
                SqlCommand cmd = new SqlCommand(update, conn);
                cmd.ExecuteNonQuery();
                MessageBox.Show("schoenen aangepast");
                SqlCommand cmda = new SqlCommand("SELECT * FROM schoenenlijst", conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmda);
                conn.Close();
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dataGridView1.DataSource = dt;
            }

        }

    }
}
