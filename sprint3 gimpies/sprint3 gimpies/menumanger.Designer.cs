﻿namespace sprint3_gimpies
{
    partial class menumanger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_beheer = new System.Windows.Forms.Button();
            this.btn_mederwerkers = new System.Windows.Forms.Button();
            this.btn_uitloggen = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.CadetBlue;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1216, 155);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("ROG Fonts", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(442, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(258, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome Admin";
            // 
            // btn_beheer
            // 
            this.btn_beheer.BackColor = System.Drawing.Color.RoyalBlue;
            this.btn_beheer.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_beheer.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_beheer.Location = new System.Drawing.Point(236, 177);
            this.btn_beheer.Name = "btn_beheer";
            this.btn_beheer.Size = new System.Drawing.Size(212, 65);
            this.btn_beheer.TabIndex = 1;
            this.btn_beheer.Text = "Schoenenbeheer";
            this.btn_beheer.UseVisualStyleBackColor = false;
            this.btn_beheer.Click += new System.EventHandler(this.btn_beheer_Click);
            // 
            // btn_mederwerkers
            // 
            this.btn_mederwerkers.BackColor = System.Drawing.Color.RoyalBlue;
            this.btn_mederwerkers.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_mederwerkers.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_mederwerkers.Location = new System.Drawing.Point(454, 177);
            this.btn_mederwerkers.Name = "btn_mederwerkers";
            this.btn_mederwerkers.Size = new System.Drawing.Size(212, 65);
            this.btn_mederwerkers.TabIndex = 2;
            this.btn_mederwerkers.Text = "Medewerkers beheer";
            this.btn_mederwerkers.UseVisualStyleBackColor = false;
            this.btn_mederwerkers.Click += new System.EventHandler(this.btn_mederwerkers_Click);
            // 
            // btn_uitloggen
            // 
            this.btn_uitloggen.BackColor = System.Drawing.Color.RoyalBlue;
            this.btn_uitloggen.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_uitloggen.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_uitloggen.Location = new System.Drawing.Point(681, 177);
            this.btn_uitloggen.Name = "btn_uitloggen";
            this.btn_uitloggen.Size = new System.Drawing.Size(212, 65);
            this.btn_uitloggen.TabIndex = 3;
            this.btn_uitloggen.Text = "Uitloggen";
            this.btn_uitloggen.UseVisualStyleBackColor = false;
            this.btn_uitloggen.Click += new System.EventHandler(this.btn_uitloggen_Click);
            // 
            // menumanger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1216, 773);
            this.Controls.Add(this.btn_uitloggen);
            this.Controls.Add(this.btn_mederwerkers);
            this.Controls.Add(this.btn_beheer);
            this.Controls.Add(this.panel1);
            this.Name = "menumanger";
            this.Text = "menumanger";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_beheer;
        private System.Windows.Forms.Button btn_mederwerkers;
        private System.Windows.Forms.Button btn_uitloggen;
    }
}