﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sprint3_gimpies
{
    public partial class menu : Form
    {
        public menu()
        {
            InitializeComponent();
        }


        private void btn_vorraad_Click(object sender, EventArgs e)
        {
            Vooraad vooraad = new Vooraad();
            vooraad.Show();
            this.Close();
        }

        private void btn_uitloggen_Click(object sender, EventArgs e)
        {
            login login = new login();
            login.Show();
            this.Close();
        }

        private void btn_verkopen_Click(object sender, EventArgs e)
        {
            Verkoopschoenen Verkoopschoenen = new Verkoopschoenen();
            Verkoopschoenen.Show();
            this.Close();
        }
    }
}
